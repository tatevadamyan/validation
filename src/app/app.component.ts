import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn} from '@angular/forms';
// import {forbiddenNameValidator} from './forbbiden-name.directive';

function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const forbidden = nameRe.test(control.value);
    return forbidden ? {'forbiddenName': {value: control.value}} : null;
  };
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  myForm: FormGroup;


    constructor(private fb: FormBuilder){
      this.createForm();
    }

    createForm(){
      this.myForm = this.fb.group({
          name: ['', [Validators.required, Validators.minLength(2),  forbiddenNameValidator(/liza/i)]],
          lastname: ['', [Validators.required, Validators.minLength(3)]],
          email:['',[Validators.required, Validators.pattern('^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$')]],
          // password:['', [Validators.required, Validators.minLength(8)]],
          // confirm:['', [Validators.required, Validators.minLength(8)],  this.duplicatePassword('password', 'confirm')]
      });
    }
    // duplicatePassword(passwordKey: string, passwordConfirmationKey: string) {
    //   return (group: FormGroup) => {
    //     let passwordInput = group.controls[passwordKey],
    //         passwordConfirmationInput = group.controls[passwordConfirmationKey];
    //     if (passwordInput.value !== passwordConfirmationInput.value) {
    //       return passwordConfirmationInput.setErrors({notEquivalent: true})
    //     }
    //     else {
    //         return passwordConfirmationInput.setErrors(null);
    //     }
    //   }
    // }

    onSubmit() {
      
    }
    
}

